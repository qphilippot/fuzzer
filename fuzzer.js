ExpressRouteExtractor = {
  split(thing) {
  if (typeof thing === "string") {
    return thing.split("/");
  } else if (thing.fast_slash) {
    return "";
  } else {
    var match = thing.toString().replace("\\/?", "").replace("(?=\\/|$)", "$").match(/^\/\^((?:\\[.*+?^${}()|[\]\\\/]|[^.*+?^${}()|[\]\\\/])*)\$\//);
    return match ? match[1].replace(/\\(.)/g, "$1").split("/") : "<complex:" + thing.toString() + ">";
  }
  },
  
  
  
  print(path = [], layer = {}, options = {}) {
  if (typeof layer.route !== "undefined") {
    layer.route.stack.forEach(middleware => ExpressRouteExtractor.print(path.concat(ExpressRouteExtractor.split(layer.route.path)), middleware, options));
  } else if (layer.name === "router" && layer.handle.stack) {
    layer.handle.stack.forEach(middleware => ExpressRouteExtractor.print(path.concat(ExpressRouteExtractor.split(layer.regexp)), middleware, options));
  } else if (typeof layer.method !== "undefined") {
    let route = "/" + path.concat(ExpressRouteExtractor.split(layer.regexp)).filter(Boolean).join("/");
    // layer.method + '-' + route ex: get-/api/agent
    // object.property
    // var propertyName = "myProp";
    // object[propertyName]
    // array.indexOf -> O(n)
    // object[propertyName] -> O(1)
    // {} -> options.routesSeen[layer.method + '-' + route] => undefined
    const key = layer.method + "-" + route;
    if (options.routesSeen[key] !== true) {
    options.routesSeen[key] = true;
    options.routes.push({hostname: options.hostname, path: route, method: layer.method, port: options.port});
    }
  }
  },
  
  extractRoute(router, options = {}) {
  let prefixs = [];
  if (typeof options.routes === "undefined") { options.routes = []; }
  if (typeof options.hostname === "undefined") { options.hostname = "localhost"; }
  if (typeof options.port === "undefined") { options.hostname = 80; }
  options.routesSeen = {};
  // pass middlewares to express routes in a static way
  router.stack.forEach(middleware => ExpressRouteExtractor.print(prefixs, middleware, options));
  return options.routes;
  }
};

module.exports = ExpressRouteExtractor;




   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   