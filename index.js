const fuzzer = require("./fuzzer");
const routes = require("./fixtures/crypto-bot.route");

console.log("nombre de routes", routes.length);

const http = require("http");
const fs = require("fs");
function log(filename, string) {
  // fs.existsSync('./log/' + filename . '.log')?fs.appendFileSync('./log/' + filename + '.log', string):console.log('didnt work');
  fs.appendFileSync("./logs/" + filename + ".log", string);
}

function sendRequest(options = {}) {
  return new Promise(resolve => {
    let allData = "";
    const req = http.request(options, res => {
      res.on("data", data => { allData += data; });
      res.on("end", () => {
        log(options.log, JSON.stringify({code: res.statusCode, request: options.method + " - " + options.path, response: allData, error: null}, null, 4));
        resolve();
      });
    });
    
    
    req.on("error", e => {
      log(options.log, JSON.stringify({code: 500, request: options.method + " - " + options.path, response: allData, error: e}, null, 4));
      resolve();
    });
    req.end();
  });
}

const fileName = Date.now();
// routes.forEach(async (routeData) => {
// 	await sendRequest({
//     log: fileName,
//     ...routeData // ... means "i assign current object with it"
//   });
// });
(async () => {
  await sendRequest({
    log: fileName,
    ...routes[0] // ... means "i assign current object with it"
  });
})();
